String provider = 'aws'
String product = 'microservices'
String serviceDisplayName = 'NumberRangeSequenceService'
String service = 'sequencer'
String basePath = 'metapackteam'
String jenkinsFiles = 'pipelines/products/microservices/sequencer'
Boolean notificatonEnabled = true
String emailRecipients = "Unicorns@metapack.com"
String slackBaseUrl = 'https://metapackgroup.slack.com/services/hooks/jenkins-ci/'
String slackNotifictionChannel = '#unicorns_info'
String slackTokenCredentialId = 'slack-unicorns-notifications'

def envConfigs = [
  'lcl':[
    'slaveLabel': 'dev-gll-playground',
    'awsAccount': 'playground',
    'devopsInfrastructureRepoBranch': 'master',
    'customers': [
		'shd': []
	],
    'dockerRepoName': 'metapackltd/number-range-sequence-service-dev'
  ],
  'int':[
    'slaveLabel': 'techops-staging-cbc',
    'awsAccount': 'staging',
    'devopsInfrastructureRepoBranch': 'master',
	'customers': [
		'shd': [
			'autoDestroyAllowed': 'true'
		],
		'internal': []
	],
    'dockerRepoName': 'metapackltd/number-range-sequence-service-dev'
  ],
  'pft':[
    'slaveLabel': 'techops-staging-cbc',
    'awsAccount': 'staging',
    'devopsInfrastructureRepoBranch': 'master',
    'customers': [
		'shd': [
			'autoDestroyAllowed': 'true'
		]
	],
    'dockerRepoName': 'metapackltd/number-range-sequence-service-dev'
  ],
  'sbx':[
    'slaveLabel': 'techops-staging-cbc',
    'awsAccount': 'staging',
    'devopsInfrastructureRepoBranch': 'master',
    'customers': [
		'shd': []
	],
    'dockerRepoName': 'metapackltd/number-range-sequence-service'
  ],
  'prd':[
    'slaveLabel': 'techops-production-cbc',
    'awsAccount': 'production',
    'devopsInfrastructureRepoBranch': 'master',
    'customers': [
		'shd': []
	],
    'dockerRepoName': 'metapackltd/number-range-sequence-service'
  ]
]

def targets = [ 'eu-west-1' ]
def jobPath = "products/${product}"
def serviceRepoAddress = "git@bitbucket.org:metapack/number-range-sequence-service.git"

folder(jobPath)

//Create sub folders
for (envConfig in envConfigs) {
    for (target in targets) {
        for (customer in envConfig.value.customers){
            folder("${jobPath}/${serviceDisplayName}")
            folder("${jobPath}/${serviceDisplayName}/${envConfig.key}"){
                displayName("${envConfig.key}")
                description("${envConfig.key} jobs")
            }
            folder("${jobPath}/${serviceDisplayName}/${envConfig.key}/${target}"){
                displayName("${target}")
                description("${envConfig.key} ${target} jobs")
            }
            folder("${jobPath}/${serviceDisplayName}/${envConfig.key}/${target}/${customer.key}"){
                displayName("${customer.key}")
                description("${envConfig.key} ${target} ${customer.key} jobs")
            }
        }
    }
}

//Create deployment job
for (envConfig in envConfigs) {
    for (target in targets) {
        for (customer in envConfig.value.customers) {
            pipelineJob("${jobPath}/${serviceDisplayName}/${envConfig.key}/${target}/${customer.key}/infrastructure") {
                displayName("Create infrastructure")
                description("Create infrastructure")

                logRotator {
                    numToKeep(10)
                }
            
                definition {
                    cpsScm {
                        scm {
                            git {
                                remote {
                                    name 'origin'
                                    url "git@bitbucket.org:metapack/cloudbees-jenkins-config.git"
                                    branch 'master'
                                    credentials 'bitbucket-credentials'
                                }
                            }
                        }
                        scriptPath("$jenkinsFiles/numberRangeSequenceService.jenkinsfile")
                    }
                    concurrentBuild(false)
                }

                environmentVariables {
                    env('ENV', envConfig.key)
                    env('REGION', target)
                    env('PRODUCT', product)
                    env('SERVICE', service)
                    env('PROVIDER', provider)
                    env('TARGET', target)
                    env('JENKINS_SLAVE_LABEL', envConfig.value.slaveLabel)
                    env('AWS_ACCOUNT', envConfig.value.awsAccount)
                    env('CUSTOMER', customer.key)
                    env('DEVOPS_INFRASTRUCTURE_REPO_BRANCH', envConfig.value.devopsInfrastructureRepoBranch)
                    env('SERVICE_REPO_ADDRESS', serviceRepoAddress)
                    env('DOCKER_REPO_NAME', envConfig.value.dockerRepoName)
                    env('NOTIFICATON_ENABLED', notificatonEnabled)
                    env('EMAIL_RECIPIENTS', emailRecipients)
                    env('SLACK_BASE_URL', slackBaseUrl)
                    env('SLACK_NOTIFICATION_CHANNEL', slackNotifictionChannel)
                    env('SLACK_TOKEN_CREDENTIAL_ID', slackTokenCredentialId)
                }

                parameters {
                    stringParam('DOCKER_TAG', '', 'A docker image to be deployed')
                    stringParam('SERVICE_REPO_BRANCH_OR_COMMIT', '', 'Required name of the branch or identifier of the commit from which docker image has been built')
                    stringParam('NRSS_MAX_CONNECTION_CACHE', '', '[OPTIONAL] Number of database connetions in connection pool')
                    stringParam('TASK_COUNT', '', '[OPTIONAL] If provided then not calculate automaticly tasks')

                    booleanParam('DEPLOY_MONITORING', false, '')
                    stringParam('MONITORING_DOCKER_IMAGE_TAG', '', 'A docker image tag to be deployed')
                    stringParam('MONITORING_SERVICE_REPO_BRANCH_OR_COMMIT', '', 'Required name of the branch or identifier of the commit from which docker image has been built')
                    stringParam('MONITORING_INTERVAL_SECONDS', '', 'Monitoring interval (A time between check sequence service)')
                    booleanParam('MONITORING_FORCE_UPDATE', false, 'Force RDS update')

                    if("${envConfig.value.awsAccount}" == "playground" || "${envConfig.value.awsAccount}" == "staging"){
                        stringParam('BRANCH_ID','','Name of enviorement to set up. Optional.')
                        booleanParam('PUBLIC_ACCESS', false, 'Environment should be publicly accessible')
                    }
                }
            }

            if (envConfig.key == "lcl" || envConfig.key == "int")
            {
                pipelineJob("${jobPath}/${serviceDisplayName}/${envConfig.key}/${target}/${customer.key}/nrs-integration-tests") {
                    displayName("Run integration tests")
                    description("Run integration tests")

                    logRotator {
                        numToKeep(10)
                    }
                
                    definition {
                        cpsScm {
                            scm {
                                git {
                                    remote {
                                        name 'origin'
                                        url "git@bitbucket.org:metapack/cloudbees-jenkins-config.git"
                                        branch 'master'
                                        credentials 'bitbucket-credentials'
                                    }
                                }
                            }
                            scriptPath("$jenkinsFiles/numberRangeSequenceServiceIntegrationTests.jenkinsfile")
                        }
                        concurrentBuild(false)
                    }

                    environmentVariables {
                        env('ENV', envConfig.key)
                        env('REGION', target)
                        env('PRODUCT', product)
                        env('SERVICE', service)
                        env('TARGET', target)
                        env('AWS_ACCOUNT', envConfig.value.awsAccount)
                        env('CUSTOMER', customer.key)
                        env('JENKINS_SLAVE_LABEL', envConfig.value.slaveLabel)
                        env('SERVICE_REPO_ADDRESS', serviceRepoAddress)
                    }

                    parameters {
                        stringParam('SERVICE_ENDPOINT', "http://prv.${customer.key}.${service}.${product}.${target}.${envConfig.key}.aws.metapack.io", 'Address of the running service to be tested')
                        stringParam('SERVICE_REPO_BRANCH_OR_COMMIT', '', 'The name of the branch or identifier of the commit from which the test definition will be taken')
                        stringParam('REPORTING_DB_ENABLED', 'true', 'Enable/Disable save tests reports to the reporting database')
                        stringParam('REPORTING_DB_JOB_ID', '', 'Job ID for identify test in reporting database')
                        stringParam('REPORTING_DB_APPLICATION_NAME', 'SEQUENCER', 'Application name for reporting database')
                        stringParam('REPORTING_DB_APPLICATION_VERSION', '', 'Application version for reporting database')
                    }
                }
            }
            
            if (envConfig.key == "lcl" || envConfig.key == "pft")
            {
                pipelineJob("${jobPath}/${serviceDisplayName}/${envConfig.key}/${target}/${customer.key}/nrs-performance-tests") {
                    displayName("Run performance tests")
                    description("Run performance tests")

                    logRotator {
                        numToKeep(10)
                    }
                
                    definition {
                        cpsScm {
                            scm {
                                git {
                                    remote {
                                        name 'origin'
                                        url "git@bitbucket.org:metapack/cloudbees-jenkins-config.git"
                                        branch 'master'
                                        credentials 'bitbucket-credentials'
                                    }
                                }
                            }
                            scriptPath("$jenkinsFiles/numberRangeSequenceServicePerformanceTests.jenkinsfile")
                        }
                        concurrentBuild(false)
                    }

                    environmentVariables {
                        env('ENV', envConfig.key)
                        env('REGION', target)
                        env('PRODUCT', product)
                        env('SERVICE', service)
                        env('PROVIDER', provider)
                        env('TARGET', target)
                        env('JENKINS_SLAVE_LABEL', envConfig.value.slaveLabel)
                        env('AWS_ACCOUNT', envConfig.value.awsAccount)
                        env('CUSTOMER', customer.key)
                        env('DEVOPS_INFRASTRUCTURE_REPO_BRANCH', envConfig.value.devopsInfrastructureRepoBranch)
                        env('SERVICE_REPO_ADDRESS', serviceRepoAddress)
                    }

                    parameters {
                        def scenarios = """[
                                {
                                    "scenarioOverrides": "-DbucketName=${envConfig.key}-eu-west-1-microservices -Dduration=1800 -Dusers=1 -DrequestsPerUserAtOnce=30 -Denvironment=SEQUENCER -Dgatling.core.directory.resources=/tmp/artifacts -Dgatling.core.directory.results=/tmp/artifacts -Dgatling.core.simulationClass=sequencerConstantLoad -Dgatling.core.directory.simulations=/mpm-tests/tests/gatling/sequencer/simulations/ -Dgatling.http.ahc.requestTimeout=5000 -DtraceIdNumber=NRS_PERFORMACE_TESTS -Dendpoint=http://prv.${customer.key}.${service}.${product}.${target}.${envConfig.key}.aws.metapack.io",
                                    "scenarioConfigFile": "/mpm-tests/tests/gatling/sequencer/simulations/sequencer_constant_load.yml"
                                },
                                {
                                    "scenarioOverrides": "-DbucketName=${envConfig.key}-eu-west-1-microservices -Dduration=1800 -Dusers=1 -DrequestsPerUserAtOnce=30 -Denvironment=SEQUENCER -Dgatling.core.directory.resources=/tmp/artifacts -Dgatling.core.directory.results=/tmp/artifacts -Dgatling.core.simulationClass=sequencerConstantLoad -Dgatling.core.directory.simulations=/mpm-tests/tests/gatling/sequencer/simulations/ -Dgatling.http.ahc.requestTimeout=5000 -DtraceIdNumber=NRS_PERFORMACE_TESTS -Dendpoint=http://prv.${customer.key}.${service}.${product}.${target}.${envConfig.key}.aws.metapack.io",
                                    "scenarioConfigFile": "/mpm-tests/tests/gatling/sequencer/simulations/sequencer_constant_load.yml"
                                },
                                {
                                    "scenarioOverrides": "-DbucketName=${envConfig.key}-eu-west-1-microservices -Dduration=1800 -Dusers=1 -DrequestsPerUserAtOnce=30 -Denvironment=SEQUENCER -Dgatling.core.directory.resources=/tmp/artifacts -Dgatling.core.directory.results=/tmp/artifacts -Dgatling.core.simulationClass=sequencerConstantLoad -Dgatling.core.directory.simulations=/mpm-tests/tests/gatling/sequencer/simulations/ -Dgatling.http.ahc.requestTimeout=5000 -DtraceIdNumber=NRS_PERFORMACE_TESTS -Dendpoint=http://prv.${customer.key}.${service}.${product}.${target}.${envConfig.key}.aws.metapack.io",
                                    "scenarioConfigFile": "/mpm-tests/tests/gatling/sequencer/simulations/sequencer_constant_load.yml"
                                },
                                {
                                    "scenarioOverrides": "-DbucketName=${envConfig.key}-eu-west-1-microservices -Dduration=1800 -Dusers=1 -DrequestsPerUserAtOnce=30 -Denvironment=SEQUENCER -Dgatling.core.directory.resources=/tmp/artifacts -Dgatling.core.directory.results=/tmp/artifacts -Dgatling.core.simulationClass=sequencerConstantLoad -Dgatling.core.directory.simulations=/mpm-tests/tests/gatling/sequencer/simulations/ -Dgatling.http.ahc.requestTimeout=5000 -DtraceIdNumber=NRS_PERFORMACE_TESTS -Dendpoint=http://prv.${customer.key}.${service}.${product}.${target}.${envConfig.key}.aws.metapack.io",
                                    "scenarioConfigFile": "/mpm-tests/tests/gatling/sequencer/simulations/sequencer_constant_load.yml"
                                },
                                {
                                    "scenarioOverrides": "-DbucketName=${envConfig.key}-eu-west-1-microservices -Dduration=1800 -Dusers=1 -DrequestsPerUserAtOnce=30 -Denvironment=SEQUENCER -Dgatling.core.directory.resources=/tmp/artifacts -Dgatling.core.directory.results=/tmp/artifacts -Dgatling.core.simulationClass=sequencerConstantLoad -Dgatling.core.directory.simulations=/mpm-tests/tests/gatling/sequencer/simulations/ -Dgatling.http.ahc.requestTimeout=5000 -DtraceIdNumber=NRS_PERFORMACE_TESTS -Dendpoint=http://prv.${customer.key}.${service}.${product}.${target}.${envConfig.key}.aws.metapack.io",
                                    "scenarioConfigFile": "/mpm-tests/tests/gatling/sequencer/simulations/sequencer_constant_load.yml"
                                },
                                {
                                    "scenarioOverrides": "-DbucketName=${envConfig.key}-eu-west-1-microservices -Dduration=1800 -Dusers=1 -DrequestsPerUserAtOnce=30 -Denvironment=SEQUENCER -Dgatling.core.directory.resources=/tmp/artifacts -Dgatling.core.directory.results=/tmp/artifacts -Dgatling.core.simulationClass=sequencerConstantLoad -Dgatling.core.directory.simulations=/mpm-tests/tests/gatling/sequencer/simulations/ -Dgatling.http.ahc.requestTimeout=5000 -DtraceIdNumber=NRS_PERFORMACE_TESTS -Dendpoint=http://prv.${customer.key}.${service}.${product}.${target}.${envConfig.key}.aws.metapack.io",
                                    "scenarioConfigFile": "/mpm-tests/tests/gatling/sequencer/simulations/sequencer_constant_load.yml"
                                },
                                {
                                    "scenarioOverrides": "-DbucketName=${envConfig.key}-eu-west-1-microservices -Dduration=1800 -Dusers=1 -DrequestsPerUserAtOnce=30 -Denvironment=SEQUENCER -Dgatling.core.directory.resources=/tmp/artifacts -Dgatling.core.directory.results=/tmp/artifacts -Dgatling.core.simulationClass=sequencerConstantLoad -Dgatling.core.directory.simulations=/mpm-tests/tests/gatling/sequencer/simulations/ -Dgatling.http.ahc.requestTimeout=5000 -DtraceIdNumber=NRS_PERFORMACE_TESTS -Dendpoint=http://prv.${customer.key}.${service}.${product}.${target}.${envConfig.key}.aws.metapack.io",
                                    "scenarioConfigFile": "/mpm-tests/tests/gatling/sequencer/simulations/sequencer_constant_load.yml"
                                },
                                {
                                    "scenarioOverrides": "-DbucketName=${envConfig.key}-eu-west-1-microservices -Dduration=1800 -Dusers=1 -DrequestsPerUserAtOnce=30 -Denvironment=SEQUENCER -Dgatling.core.directory.resources=/tmp/artifacts -Dgatling.core.directory.results=/tmp/artifacts -Dgatling.core.simulationClass=sequencerConstantLoad -Dgatling.core.directory.simulations=/mpm-tests/tests/gatling/sequencer/simulations/ -Dgatling.http.ahc.requestTimeout=5000 -DtraceIdNumber=NRS_PERFORMACE_TESTS -Dendpoint=http://prv.${customer.key}.${service}.${product}.${target}.${envConfig.key}.aws.metapack.io",
                                    "scenarioConfigFile": "/mpm-tests/tests/gatling/sequencer/simulations/sequencer_constant_load.yml"
                                },
                                {
                                    "scenarioOverrides": "-DbucketName=${envConfig.key}-eu-west-1-microservices -Dduration=1800 -Dusers=1 -DrequestsPerUserAtOnce=30 -Denvironment=SEQUENCER -Dgatling.core.directory.resources=/tmp/artifacts -Dgatling.core.directory.results=/tmp/artifacts -Dgatling.core.simulationClass=sequencerConstantLoad -Dgatling.core.directory.simulations=/mpm-tests/tests/gatling/sequencer/simulations/ -Dgatling.http.ahc.requestTimeout=5000 -DtraceIdNumber=NRS_PERFORMACE_TESTS -Dendpoint=http://prv.${customer.key}.${service}.${product}.${target}.${envConfig.key}.aws.metapack.io",
                                    "scenarioConfigFile": "/mpm-tests/tests/gatling/sequencer/simulations/sequencer_constant_load.yml"
                                },
                                {
                                    "scenarioOverrides": "-DbucketName=${envConfig.key}-eu-west-1-microservices -Dduration=1800 -Dusers=1 -DrequestsPerUserAtOnce=30 -Denvironment=SEQUENCER -Dgatling.core.directory.resources=/tmp/artifacts -Dgatling.core.directory.results=/tmp/artifacts -Dgatling.core.simulationClass=sequencerConstantLoad -Dgatling.core.directory.simulations=/mpm-tests/tests/gatling/sequencer/simulations/ -Dgatling.http.ahc.requestTimeout=5000 -DtraceIdNumber=NRS_PERFORMACE_TESTS -Dendpoint=http://prv.${customer.key}.${service}.${product}.${target}.${envConfig.key}.aws.metapack.io",
                                    "scenarioConfigFile": "/mpm-tests/tests/gatling/sequencer/simulations/sequencer_constant_load.yml"
                                }
                            ]"""
                        if("${envConfig.value.awsAccount}" == "playground"){
                            stringParam('ENDPOINT', "http://prv.${customer.key}.${service}.${product}.${target}.${envConfig.key}.aws.metapack.io", 'Endpoint')
                            stringParam('SERVICE_REPO_BRANCH_OR_COMMIT', '', 'Required name of the branch or identifier of the commit from sequence service repository')
                            stringParam('EXECUTOR_TYPE', 'Gatling', 'Executor type')
                            stringParam('BRANCH', 'master', 'Branch')
                            textParam('SCENARIOS', scenarios, 'Scenarios')
                        } else if("${envConfig.value.awsAccount}" == "staging"){
                            stringParam('ENDPOINT', "http://prv.${customer.key}.${service}.${product}.${target}.${envConfig.key}.aws.metapack.io", 'Endpoint')
                            stringParam('SERVICE_REPO_BRANCH_OR_COMMIT', '', 'Required name of the branch or identifier of the commit from sequence service repository')
                            stringParam('EXECUTOR_TYPE', 'Gatling', 'Executor type')
                            stringParam('DOCKER_IMAGE_TAG', 'latest', 'Tag of the docker image for running the tests')
                            stringParam('TEST_DESCRIPTION', 'NRS performance tests', 'Describe why you are running these tests. Example: "Production data, NewRelic Off, 100 Users"')
                            textParam('SCENARIOS', scenarios, 'Scenarios')
                        }
                    }
                }
            }

			pipelineJob("${jobPath}/${serviceDisplayName}/${envConfig.key}/${target}/${customer.key}/change-test") {
                    displayName("Change test11")
                    description("Change test11")

                    logRotator {
                        numToKeep(10)
                    }
                
                    definition {
                        cpsScm {
                            scm {
                                git {
                                    remote {
                                        name 'origin'
                                        url "git@bitbucket.org:metapack/cloudbees-jenkins-config.git"
                                        branch 'master'
                                        credentials 'bitbucket-credentials'
                                    }
                                }
                            }
                            scriptPath("$jenkinsFiles/numberRangeSequenceServiceAutomaticDestroy.jenkinsfile")
                        }
                        concurrentBuild(true)
                    }

                    triggers {
                        cron('0 19 * * *')
                    }

                    environmentVariables {
                        env('JENKINS_SLAVE_LABEL', envConfig.value.slaveLabel)
                        env('CUSTOMER', customer.key)
						env('REGION', target)
						env('PRODUCT', product)
						env('ENV', envConfig.key)
						env('SERVICE_DISPLAY_NAME', serviceDisplayName)
						env('AWS_ACCOUNT', envConfig.value.awsAccount)
                        env('SERVICE_REPO_ADDRESS', serviceRepoAddress)
						env('AUTO_DESTROY_ALLOWED', customer.value.autoDestroyAllowed)
                    }

                    parameters {
                        stringParam('SERVICE_REPO_BRANCH_OR_COMMIT', 'master', 'Branch or identifier of the commit from which terraform will be used')
                    }
                }

            pipelineJob("${jobPath}/${serviceDisplayName}/${envConfig.key}/${target}/${customer.key}/destroy-infrastructure") {
                displayName("Destroy infrastructure")
                description("Destroy infrastructure")

                logRotator {
                    numToKeep(10)
                }
            
                definition {
                    cpsScm {
                        scm {
                            git {
                                remote {
                                    name 'origin'
                                    url "git@bitbucket.org:metapack/cloudbees-jenkins-config.git"
                                    branch 'master'
                                    credentials 'bitbucket-credentials'
                                }
                            }
                        }
                        if(envConfig.key == 'prd' || envConfig.key == 'sbx'){
                            scriptPath("$jenkinsFiles/numberRangeSequenceServicePrdSbxDestroy.jenkinsfile")
                        }
                        else{
                            scriptPath("$jenkinsFiles/numberRangeSequenceServiceDestroy.jenkinsfile")
                        }
                    }
                    concurrentBuild(false)
                }

                environmentVariables {
                    env('ENV', envConfig.key)
                    env('REGION', target)
                    env('PRODUCT', product)
                    env('SERVICE', service)
                    env('PROVIDER', provider)
                    env('TARGET', target)
                    env('JENKINS_SLAVE_LABEL', envConfig.value.slaveLabel)
                    env('AWS_ACCOUNT', envConfig.value.awsAccount)
                    env('CUSTOMER', customer.key)
                    env('DEVOPS_INFRASTRUCTURE_REPO_BRANCH', envConfig.value.devopsInfrastructureRepoBranch)
                    env('SERVICE_REPO_ADDRESS', serviceRepoAddress)
                    env('NOTIFICATON_ENABLED', notificatonEnabled)
                    env('EMAIL_RECIPIENTS', emailRecipients)
                    env('SLACK_BASE_URL', slackBaseUrl)
                    env('SLACK_NOTIFICATION_CHANNEL', slackNotifictionChannel)
                    env('SLACK_TOKEN_CREDENTIAL_ID', slackTokenCredentialId)
                }

                parameters {
                    stringParam('SERVICE_REPO_BRANCH_OR_COMMIT', '', 'Required name of the branch or identifier of the commit from which docker image has been built')
                    booleanParam('AUTO_APPROVE', false, 'Automatic approval of destroying an environment')
                    if("${envConfig.value.awsAccount}" == "playground"){
                        stringParam('BRANCH_ID','','Name of enviorement to set up. Optional.')
                        booleanParam('DESTROY_DASHBOARDS', true, 'Destroy dashboards')
                    }else{
                        booleanParam('DESTROY_DASHBOARDS', false, 'Destroy dashboards')                    
                    }
                    if(envConfig.key == 'prd' || envConfig.key == 'sbx'){
                        booleanParam('DESTROY_RDS',false,'Selecting this checkbox will destroy the RDS and database (only if deletion protection is removed)')
                    }
                    booleanParam('DESTROY_MONITORING', true, '')
                    stringParam('MONITORING_SERVICE_REPO_BRANCH_OR_COMMIT', '', 'Required name of the branch or identifier of the commit from which docker image has been built')
                    booleanParam('MONITORING_DESTROY_RDS', true, 'Selecting this checkbox will destroy the RDS and database (only if deletion protection is removed')
                }
            }

            if (envConfig.key == 'prd' && customer.key == 'shd')
            {
                pipelineJob("${jobPath}/${serviceDisplayName}/${envConfig.key}/${target}/${customer.key}/continuous-delivery") {
                    displayName("Production continuous delivery")
                    description("Runs integration, performance tests and creates docker image ready for deploy on production.")

                    logRotator {
                        numToKeep(10)
                    }
                
                    definition {
                        cpsScm {
                            scm {
                                git {
                                    remote {
                                        name 'origin'
                                        url "git@bitbucket.org:metapack/cloudbees-jenkins-config.git"
                                        branch 'master'
                                        credentials 'bitbucket-credentials'
                                    }
                                }
                            }
                            scriptPath("$jenkinsFiles/numberRangeSequenceServiceContinuousDelivery.jenkinsfile")
                        }
                        concurrentBuild(false)
                    }

                    environmentVariables {
                        env('JENKINS_SLAVE_LABEL', envConfig.value.slaveLabel)
                        env('CUSTOMER', customer.key)
                        env('SERVICE_REPO_ADDRESS', serviceRepoAddress)
                        env('AWS_ACCOUNT', envConfig.value.awsAccount)
                        env('NOTIFICATON_ENABLED', notificatonEnabled)
                        env('EMAIL_RECIPIENTS', emailRecipients)
                        env('SLACK_BASE_URL', slackBaseUrl)
                        env('SLACK_NOTIFICATION_CHANNEL', slackNotifictionChannel)
                        env('SLACK_TOKEN_CREDENTIAL_ID', slackTokenCredentialId)
                    }

                    parameters {
                        stringParam('VERSION_MAJOR', '', 'Required major part of version used to resolve a docker image to be used')
                        stringParam('VERSION_MINOR', '', 'Required minor part of version used to resolve a docker image to be used')
                        stringParam('VERSION_PATCH', '', 'Required patch part of version used to resolve a docker image to be used')
                        stringParam('VERSION_BUILD', '', 'Required build number part of version used to resolve a docker image to be used')
                        stringParam('SERVICE_REPO_BRANCH_OR_COMMIT', '', 'Required name of the branch or identifier of the commit from which docker image has been built')
						booleanParam('DESTROY_INT_ENV',false,'Destroy INT environment after build (Automatic destroy for this environments is triggered everyday at 20:00)')
						booleanParam('DESTROY_PFT_ENV',false,'Destroy PFT environment after build (Automatic destroy for this environments is triggered everyday at 20:00)')
                    }
                }
            }
			
			if (customer.value.autoDestroyAllowed)
            {
                pipelineJob("${jobPath}/${serviceDisplayName}/${envConfig.key}/${target}/${customer.key}/automatic-destroy") {
                    displayName("Automatic destroy infrastructure trigger")
                    description("Automatic destroy infrastructure for on demand environments. Triggered everyday at 20:00")

                    logRotator {
                        numToKeep(10)
                    }
                
                    definition {
                        cpsScm {
                            scm {
                                git {
                                    remote {
                                        name 'origin'
                                        url "git@bitbucket.org:metapack/cloudbees-jenkins-config.git"
                                        branch 'master'
                                        credentials 'bitbucket-credentials'
                                    }
                                }
                            }
                            scriptPath("$jenkinsFiles/numberRangeSequenceServiceAutomaticDestroy.jenkinsfile")
                        }
                        concurrentBuild(true)
                    }

                    triggers {
                        cron('0 19 * * *')
                    }

                    environmentVariables {
                        env('JENKINS_SLAVE_LABEL', envConfig.value.slaveLabel)
                        env('CUSTOMER', customer.key)
						env('REGION', target)
						env('PRODUCT', product)
						env('ENV', envConfig.key)
						env('SERVICE_DISPLAY_NAME', serviceDisplayName)
						env('AWS_ACCOUNT', envConfig.value.awsAccount)
                        env('SERVICE_REPO_ADDRESS', serviceRepoAddress)
						env('AUTO_DESTROY_ALLOWED', customer.value.autoDestroyAllowed)
                    }

                    parameters {
                        stringParam('SERVICE_REPO_BRANCH_OR_COMMIT', 'master', 'Branch or identifier of the commit from which terraform will be used')
                    }
                }
            }
        }
    }
}