job('task2_clone_repository') {
    scm {
        git {
			branch("*/master")
			remote {
				url("https://bitbucket.org/enowe/demoapp.git")
			}
		}
    }
    triggers {
        cron('H/2 * * * *')
    }
    publishers {
        downstreamParameterized {
            trigger('task2_second_job') {
                condition('UNSTABLE_OR_BETTER')
                parameters {
                    predefinedProp("SHA", "\$GIT_COMMIT")
                }
            }
        }
    }
}

job('task2_second_job'){
    parameters {
        stringParam('SHA', '')
    }
    steps {
        shell('''echo "SHA: $SHA"''')
    }
  	properties {
      rebuild {
      	autoRebuild(true)
      }
  	}
}

listView('task2') {
    jobs {
        name('task2')
        regex(/^task2.*$+/)
    }
  	jobFilters {
        status {
            status(Status.UNSTABLE)
        }
    }
    columns {
        status()
        weather()
        name()
        lastSuccess()
        lastFailure()
        lastDuration()
        buildButton()
    }
}
